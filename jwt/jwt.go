package jwt

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

const SecretKey = "tWvzj%TwJReOJuCq%UKo9n*v^4gO)*HndGvT@rOAXUZ$UzLrRXVE@tqQX5bwIqMV4pOmrr)MdN@^fv2(Zq@RyhQkUY"

func CreateToken(c *gin.Context) error {
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    SecretKey,
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	})

	token, err := claims.SignedString([]byte(SecretKey))
	if err != nil {
		return err
	}

	cookie := &http.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * 24),
		HttpOnly: true,
	}
	http.SetCookie(c.Writer, cookie)
	return nil
}

func VerificationToken(c *gin.Context) bool {
	cookie, _ := c.Cookie("jwt")

	_, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})

	if err != nil {
		return false
	}
	return true
}
