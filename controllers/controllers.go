package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	fmt.Println("router -> controllers -> Home")
	c.JSON(200, gin.H{
		"message": "index",
	})
}
