package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Router struct {
		Addr string `yaml:"addr"`
	} `yaml:"router"`
}

func Init() {
	config := &Config{}
	if err := config.Load(); err != nil {
		err := fmt.Errorf("config err -> %s", err)
		fmt.Println(err.Error())
	}
}

func (c *Config) Load() error {
	file, err := os.Open("config.yaml")
	if err != nil {
		return err
	}
	defer file.Close()

	decoder := yaml.NewDecoder(file)
	if err := decoder.Decode(&c); err != nil {
		return err
	}
	return nil
}
