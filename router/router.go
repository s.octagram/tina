package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"tina-go/controllers"
)

type Router struct {
	Addr   string
	Engine *gin.Engine
}

func Run(addr string) {
	router := &Router{
		Addr:   addr,
		Engine: gin.Default(),
	}

	router.Setup()

	if err := router.Engine.Run(router.Addr); err != nil {
		fmt.Errorf("router err -> %s", err)
	}
}

func (r *Router) Setup() {
	r.Engine.GET("/", controllers.Index)
	r.Engine.GET("/login", controllers.Login)
	r.Engine.GET("/token", controllers.Token)
	/*r.Engine.GET("/login", controllers.Login)
	r.Engine.GET("/user", controllers.User)*/
}
