package main

import (
	"tina-go/config"
	"tina-go/router"
)

var cfg *config.Config

func main() {
	config.Init()
	//router.Run(cfg.Router.Addr)
	router.Run(":8080")
}
